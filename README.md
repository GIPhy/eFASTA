# eFASTA

_eFASTA_ is a command line program written in [Java](https://docs.oracle.com/javase/8/docs/technotes/guides/language/index.html) that allows extracting a nucleotide segment (e.g. region, ORF, CDS) from a FASTA file.

## Compilation and execution

The source code of _eFASTA_ is inside the _src_ directory and could be compiled and executed in two different ways. 

#### Building an executable jar file

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/eFASTA.git
```
On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (6 or higher) installed, a Java executable jar file can be created. In a command-line window, go to the _src_ directory and type:
```bash
javac EFASTA.java 
echo Main-Class: EFASTA > MANIFEST.MF 
jar -cmvf MANIFEST.MF EFASTA.jar EFASTA.class 
rm MANIFEST.MF EFASTA.class 
```
This will create the executable jar file `EFASTA.jar` that could be run with the following command line model:
```bash
java -jar EFASTA.jar [options]
```

#### Building a native code binary

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/eFASTA.git
```
On computers with [GraalVM](https://www.graalvm.org/downloads/) installed, a native executable can be built. In a command-line window, go to the _src_ directory, and type:
```bash
javac EFASTA.java 
native-image EFASTA eFASTA
rm EFASTA.class
```
This will create the native executable `eFASTA` that can be run with the following command line model:
```bash
./eFASTA [options]
```


## Usage

Run _eFASTA_ without option to read the following documentation:

```
 eFASTA

 USAGE:   eFASTA <options>

  where options are:

   -fasta, -f <infile>             (mandatory) FASTA-formatted  nucleotide  sequence file name
   -coord, -c <pattern:start-end>  (mandatory) DNA segment  to extract  from [infile]  defined
                                   by the first  FASTA  header  containing  [pattern]  and the
                                   region  comprised  between nucleotide  indexes [start]  and
                                   [end] (both inclusive);  if [start] > [end],  the extracted
                                   nucleotide segment is reverse-complemented
   -outname, -o <basename>         basename of the output file;  extracted nucleotide sequence
                                   will be written into [basename].fna (default: "seq")
   -cds                            to  indicate  that the  extracted   DNA  segment is  a CDS,
                                   leading to  the  writing  of  its  amino  acid  translation
                                   (standard  genetic  code)  into  a  second  FASTA-formatted
                                   output file [basename].faa
   -fcds                           to search for  the full CDS  (starting and  ending by START
                                   and  STOP codons,  respectively)  that includes  the region
                                   specified by option -c
   -orf                            to search for the ORF  (starting and ending by STOP codons)
                                   that includes the region specified by option -c
   -start, -init <string>          (only with options -cds  and -fcds) several alternate START
                                   codons (non ATG) could be specified with this option:
                                    PROK    = ATG GTG TTG
                                    PROK+   = ATG GTG TTG CTG ATT
                                    PROK++  = ATG GTG TTG CTG ATT ATC ATA
   -stop                           (only with options -cds, -fcds or -orf) to include the STOP
                                   codon in the outputed sequences
```

## Example

The directory _example_ contains the FASTA-formatted file _Ecoli.O104H4.plsm.fna_. This file contains the three plasmid sequences pAA-EA11,	pESBL-EA11 and pG-EA11 of the bacteria _Escherichia coli_ O104:H4 _str._ 2011C-3493 (accession numbers: CP003291, CP003290 and CP003292, respectively). As a tblastn search against these plasmid sequences indicates that the region comprised between the nucleotide indexes 31400 and 31293 of CP003291 (therefore reverse-complement) is significantly similar to the query amino-acid sequence, _eFASTA_ could be very useful to extract this likely coding region.

##### Standard extraction
```bash
eFASTA  -f Ecoli.O104H4.plsm.fna  -c CP003291:31400-31293
```
This command line creates the file _seq.fna_ containing the following nucleotide segment:
```
>CP003291.1 plasmid pAA-EA11::31400-31293
GGGCTGATCGGCACCTGCCGTCTGAACGGTATCGATCCGGAAGCGTATCTGCGCCATATTCTGAGCGTACTGCCGGAATGGCCTTCCAACCGAGTTGGCGAACTCCTG
```

##### Open Reading Frame (ORF)
```bash
eFASTA  -f Ecoli.O104H4.plsm.fna  -c CP003291:31400-31293  -orf
```
This command line creates the file _seq.fna_ with the smallest ORF containing the specified region:
```
>CP003291.1 plasmid pAA-EA11::31433-31266
GTACAGGGCTGGAGCTTGTGTGCACTGCTGTACGGGCTGATCGGCACCTGCCGTCTGAACGGTATCGATCCGGAAGCGTATCTGCGCCATATTCTGAGCGTACTGCCGGAATGGCCTTCCAACCGAGTTGGCGAACTCCTGCCATGGAACGTAGTACTCACCAATAAA
```
as well as  the file _seq.faa_ containing its translation (standard genetic code):
```
>CP003291.1 plasmid pAA-EA11::31433-31266
VQGWSLCALLYGLIGTCRLNGIDPEAYLRHILSVLPEWPSNRVGELLPWNVVLTNK
```

##### Open Reading Frame (ORF) with codons STOP
```bash
eFASTA  -f Ecoli.O104H4.plsm.fna  -c CP003291:31400-31293  -orf  -stop
```
This command line creates the two files _seq.fna_ and _seq.faa_ containing the ORF and its translation, respectively, with the two ending-up codons STOP:
```
>CP003291.1 plasmid pAA-EA11::31436-31263
TGAGTACAGGGCTGGAGCTTGTGTGCACTGCTGTACGGGCTGATCGGCACCTGCCGTCTGAACGGTATCGATCCGGAAGCGTATCTGCGCCATATTCTGAGCGTACTGCCGGAATGGCCTTCCAACCGAGTTGGCGAACTCCTGCCATGGAACGTAGTACTCACCAATAAATAA
```
```
>CP003291.1 plasmid pAA-EA11::31436-31263
?VQGWSLCALLYGLIGTCRLNGIDPEAYLRHILSVLPEWPSNRVGELLPWNVVLTNK?
```

##### Coding Sequence (CDS)
```bash
eFASTA  -f Ecoli.O104H4.plsm.fna  -c CP003291:31400-31293  -fcds
```
This command line creates the file _seq.fna_ with the the smallest putative CDS containing the specified region:
```
>CP003291.1 plasmid pAA-EA11::31433-31266
GTACAGGGCTGGAGCTTGTGTGCACTGCTGTACGGGCTGATCGGCACCTGCCGTCTGAACGGTATCGATCCGGAAGCGTATCTGCGCCATATTCTGAGCGTACTGCCGGAATGGCCTTCCAACCGAGTTGGCGAACTCCTGCCATGGAACGTAGTACTCACCAATAAA
```
as well as the file _seq.faa_ containing its translation (standard genetic code):
```
>CP003291.1 plasmid pAA-EA11::31433-31266
VQGWSLCALLYGLIGTCRLNGIDPEAYLRHILSVLPEWPSNRVGELLPWNVVLTNK
```
Of note, it is the same results as with option `-orf` because the STOP codon TGA is occurring first (before any START codon ATG).

##### Coding Sequence (CDS) with alternate codon START
```bash
eFASTA  -f Ecoli.O104H4.plsm.fna  -c CP003291:31400-31293  -fcds  -start PROK
```
This command line creates the two files _seq.fna_ and _seq.faa_ containing the CDS and its translation, respectively, by considering each of the three codons ATG, GTG and TTG as a putative codon START:
```
>CP003291.1 plasmid pAA-EA11::31418-31266
TTGTGTGCACTGCTGTACGGGCTGATCGGCACCTGCCGTCTGAACGGTATCGATCCGGAAGCGTATCTGCGCCATATTCTGAGCGTACTGCCGGAATGGCCTTCCAACCGAGTTGGCGAACTCCTGCCATGGAACGTAGTACTCACCAATAAA
```
```
>CP003291.1 plasmid pAA-EA11::31418-31266
MCALLYGLIGTCRLNGIDPEAYLRHILSVLPEWPSNRVGELLPWNVVLTNK
```

##### Coding Sequence (CDS) with alternate codons START and codon STOP
```bash
eFASTA  -f Ecoli.O104H4.plsm.fna  -c CP003291:31400-31293  -fcds  -start PROK  -stop
```
This command line creates the two files _seq.fna_ and _seq.faa_ containing the CDS and its translation, respectively, by considering each of the three codons ATG, GTG and TTG as a putative codon START, and including the termination codon:
```
>CP003291.1 plasmid pAA-EA11::31418-31263
TTGTGTGCACTGCTGTACGGGCTGATCGGCACCTGCCGTCTGAACGGTATCGATCCGGAAGCGTATCTGCGCCATATTCTGAGCGTACTGCCGGAATGGCCTTCCAACCGAGTTGGCGAACTCCTGCCATGGAACGTAGTACTCACCAATAAATAA
```
```
>CP003291.1 plasmid pAA-EA11::31418-31263
MCALLYGLIGTCRLNGIDPEAYLRHILSVLPEWPSNRVGELLPWNVVLTNK?
```



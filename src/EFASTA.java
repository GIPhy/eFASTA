/*
  ########################################################################################################

  EFASTA: extracting nucleotide segments from FASTA file
  
  Copyright (C) 2017-2020  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.util.*;

public class EFASTA {
    //### constants ##########
    final static String VERSION = "1.2b.201024ac";
    final static String[] CODON =   {"AAA" , "AAC" , "AAG" , "AAT" ,  // A A -
				     "ACA" , "ACC" , "ACG" , "ACT" ,  // . C -
				     "AGA" , "AGC" , "AGG" , "AGT" ,  // . G -
				     "ATA" , "ATC" , "ATG" , "ATT" ,  // . T -
				     "CAA" , "CAC" , "CAG" , "CAT" ,  // C A -
				     "CCA" , "CCC" , "CCG" , "CCT" ,  // . C -
				     "CGA" , "CGC" , "CGG" , "CGT" ,  // . G -
				     "CTA" , "CTC" , "CTG" , "CTT" ,  // . T -
				     "GAA" , "GAC" , "GAG" , "GAT" ,  // G A -
				     "GCA" , "GCC" , "GCG" , "GCT" ,  // . C -
				     "GGA" , "GGC" , "GGG" , "GGT" ,  // . G -
				     "GTA" , "GTC" , "GTG" , "GTT" ,  // . T -
				     "TAA" , "TAC" , "TAG" , "TAT" ,  // T A -
				     "TCA" , "TCC" , "TCG" , "TCT" ,  // . C -
				     "TGA" , "TGC" , "TGG" , "TGT" ,  // . G -
				     "TTA" , "TTC" , "TTG" , "TTT" }; // . T -
    final static char[] AMINO_ACID = {'K'  ,  'N'  ,  'K'  ,  'N'  ,  // A A -
				      'T'  ,  'T'  ,  'T'  ,  'T'  ,  // . C -
				      'R'  ,  'S'  ,  'R'  ,  'S'  ,  // . G -
				      'I'  ,  'I'  ,  'M'  ,  'I'  ,  // . T -  ('M' = START)
				      'Q'  ,  'H'  ,  'Q'  ,  'H'  ,  // C A -
				      'P'  ,  'P'  ,  'P'  ,  'P'  ,  // . C -
				      'R'  ,  'R'  ,  'R'  ,  'R'  ,  // . G -
				      'L'  ,  'L'  ,  'L'  ,  'L'  ,  // . T -
				      'E'  ,  'D'  ,  'E'  ,  'D'  ,  // G A -
				      'A'  ,  'A'  ,  'A'  ,  'A'  ,  // . C -
				      'G'  ,  'G'  ,  'G'  ,  'G'  ,  // . G -
				      'V'  ,  'V'  ,  'V'  ,  'V'  ,  // . T -
				      '?'  ,  'Y'  ,  '?'  ,  'Y'  ,  // T A -  ('?' for STOP)
				      'S'  ,  'S'  ,  'S'  ,  'S'  ,  // . C -
				      '?'  ,  'C'  ,  'W'  ,  'C'  ,  // . G -  ('?' for STOP)
				      'L'  ,  'F'  ,  'L'  ,  'F'  }; // . T -
    final static int FLGT = 500000;
    final static String[] CSTOP        = {"TAA", "TAG", "TGA"};
    final static String[] CSTART       = {"ATG"};
    // => prokaryote alternate start codon lists derived from Hecht et al. (2017) doi:10.1093/nar/gkx070
    final static String[] CSTART_PRO   = {"ATG", "GTG", "TTG"};
    final static String[] CSTART_PROK  = {"ATG", "GTG", "TTG", "CTG", "ATT"};
    final static String[] CSTART_PROKA = {"ATG", "GTG", "TTG", "CTG", "ATT", "ATC", "ATA"};
    //### options ##########
    static File dnaFile;                  // -fasta
    static String fh;                     // -coord
    static int begin, end;                // -coord
    static boolean cds, fcds, orf, stop;  // -cds, -fcds, -orf, -stop
    static String basename;               // -outname
    //### io ##########
    static BufferedReader in;
    static BufferedWriter out;
    //### data ##########
    static String[] start;
    static StringBuilder dnaSeq;
    static String dna;
    //### stuffs ##########
    static int b, o, lgt;
    static boolean ok;
    static String line, c;
    
    public static void main(String[] args) throws IOException {

	// ###########################
	// ##### documentation   #####
	// ###########################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" eFASTA v." + VERSION + "                              Copyright (C) 2017-2020  Institut Pasteur");
	    System.out.println("");
	    System.out.println(" USAGE:   eFASTA  [options]");
	    System.out.println("");
	    System.out.println("  where options are:");
	    System.out.println("");
	    System.out.println("   -fasta, -f <infile>             (mandatory) FASTA-formatted  nucleotide  sequence file name");
	    System.out.println("   -coord, -c <pattern:start-end>  (mandatory) DNA segment  to extract  from [infile]  defined");
	    System.out.println("                                   by the first  FASTA  header  containing  [pattern]  and the");
	    System.out.println("                                   region  comprised  between nucleotide  indexes [start]  and");
	    System.out.println("                                   [end] (both inclusive);  if [start] > [end],  the extracted");
	    System.out.println("                                   nucleotide segment is reverse-complemented");
	    System.out.println("   -outname, -o <basename>         basename of the output file;  extracted nucleotide sequence");
	    System.out.println("                                   will be written into [basename].fna (default: \"seq\")");
	    System.out.println("   -cds                            to  indicate  that the  extracted   DNA  segment is  a CDS,");
	    System.out.println("                                   leading to  the  writing  of  its  amino  acid  translation");
	    System.out.println("                                   (standard  genetic  code)  into  a  second  FASTA-formatted");
	    System.out.println("                                   output file [basename].faa");
	    System.out.println("   -fcds                           to search for  the full CDS  (starting and  ending by START");
	    System.out.println("                                   and  STOP codons,  respectively)  that includes  the region");
	    System.out.println("                                   specified by option -c");
	    System.out.println("   -orf                            to search for the ORF  (starting and ending by STOP codons)");
	    System.out.println("                                   that includes the region specified by option -c");
	    System.out.println("   -start, -init <string>          (only with options -cds  and -fcds) several alternate START");
	    System.out.println("                                   codons (non ATG) could be specified with this option:");
	    System.out.println("                                    PROK    = ATG GTG TTG");
	    System.out.println("                                    PROK+   = ATG GTG TTG CTG ATT");
	    System.out.println("                                    PROK++  = ATG GTG TTG CTG ATT ATC ATA");
	    System.out.println("   -stop                           (only with options -cds, -fcds or -orf) to include the STOP");
	    System.out.println("                                   codon in the outputed sequences");
	    System.out.println("");
	    System.exit(0);
	}
	
	// ###########################
	// ##### reading options #####
	// ###########################
	dnaFile = new File("N.o.F.I.L.E");
	fh = "N.o.F.H";
	begin = 0;
	end = 1;
	cds = fcds = orf = stop = false;
	start = Arrays.copyOf(CSTART, 1);
	basename = "seq";
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-fasta") || args[o].equals("-f") ) {
		dnaFile = new File( args[++o] );
		if ( ! dnaFile.exists() ) { System.out.println(" incorrect file name: " + dnaFile.toString() + " (option -f)"); System.exit(1); }
		continue;
	    }
	    if ( args[o].equals("-coord") || args[o].equals("-c") ) {
		line = args[++o];
		if ( line.endsWith(":") || line.endsWith("-") ) { System.out.println(" incorrect pattern: " + line + " (option -c)"); System.exit(1); }
		if ( (b=line.indexOf(":")) == -1 ) { System.out.println(" missing \":\": " + line + " (option -c)"); System.exit(1); }
		fh = line.substring(0 , b); line = line.substring(++b);
		if ( (b=line.indexOf("-")) == -1 ) { System.out.println(" missing \"-\": " + line + " (option -c)"); System.exit(1); }
		try { begin = Integer.parseInt( line.substring(0 , b) ); end = Integer.parseInt( line.substring(++b) ); }
		catch ( NumberFormatException e ) { System.out.println(" incorrect pattern: " + line + " (option -c)"); System.exit(1); }
		continue;
	    }
	    if ( args[o].equals("-outname") || args[o].equals("-o") ) { basename = args[++o]; continue; }
	    if ( args[o].equals("-cds") )  { cds = true; orf = false; continue; }
	    if ( args[o].equals("-fcds") ) { cds = fcds = true; continue; }
	    if ( args[o].equals("-orf") )  { orf = true; cds = fcds = false; continue; }
	    if ( args[o].equals("-stop") ) { stop = true; continue; }
	    if ( args[o].equals("-init") || args[o].equals("-start") ) {
		line = args[++o].toUpperCase();
		if ( line.equals("PROK") )   { start = Arrays.copyOf(CSTART_PRO,   CSTART_PRO.length);   continue; }
		if ( line.equals("PROK+") )  { start = Arrays.copyOf(CSTART_PROK,  CSTART_PROK.length);  continue; }
		if ( line.equals("PROK++") ) { start = Arrays.copyOf(CSTART_PROKA, CSTART_PROKA.length); continue; }
	    }
	}
	if ( dnaFile.toString().equals("N.o.F.I.L.E") ) { System.out.println(" missing file name (option -f)"); System.exit(1); }
	if ( fh.equals("N.o.F.H") ) { System.out.println(" missing coordinates (option -c)"); System.exit(1); }
	Arrays.sort(start);
	
	// ##################################
	// ##### reading dna fasta file #####
	// ##################################
	in = new BufferedReader(new FileReader(dnaFile));
	line = "";
	while ( (! line.startsWith(">")) || (line.indexOf(fh) == -1) ) 
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); System.out.println(fh + " not found inside " + dnaFile.toString()); System.exit(1); }
	fh = line;
 	dnaSeq = new StringBuilder(""); lgt = ( begin < end ) ? end : begin; lgt += ( fcds || orf ) ? FLGT : 0;
	while ( dnaSeq.length() < lgt ) {
	    try { if ( (line=in.readLine().trim()).startsWith(">") ) { in.close(); break; } } catch ( NullPointerException e ) { in.close(); break; }
	    dnaSeq = dnaSeq.append(line.toUpperCase());
	}
	if ( lgt < dnaSeq.length() ) lgt = dnaSeq.length();

	// ##################################
	// ##### extracting DNA segment #####
	// ##################################
	//### no CDS, no ORF ################################################
	if ( ! (cds || orf) ) {
	    dna = ( begin < end ) ? dnaSeq.substring(begin-1 , end) : rc( dnaSeq.substring(end-1, begin) );

	    fh = fh + "::" + begin + "-" + end;
	    out = new BufferedWriter(new FileWriter(new File(basename + ".fna"))); out.write(fh); out.newLine(); out.write(dna); out.newLine(); out.close();

	    System.exit(0);
	}

	//### CDS/ORF expected ##############################################
	if ( begin < end ) { 
	    dna = dnaSeq.substring(--begin , end); lgt = dnaSeq.length() - 2; 
	    //### ORF ######################
	    if ( orf ) {
		//## looking for a first stop codon ###
		while ( (begin >= 3) && ! isSTOP(fc(dna)) ) dna = dnaSeq.substring((begin -= 3), end);
		if ( (! stop) && isSTOP(fc(dna)) ) dna = dnaSeq.substring((begin += 3), end);
		//## looking for a last stop codon ####
		while ( (end < lgt) && ! isSTOP(lc(dna)) ) dna = dnaSeq.substring(begin, (end += 3)); 
	    }
	    //### complete CDS #############
	    if ( fcds ) {
		//## looking for a start codon ###
		while ( (begin >= 3) && (! isin(c=fc(dna), start)) && ! isSTOP(c) ) dna = dnaSeq.substring((begin -= 3), end); 
		if ( isSTOP(fc(dna)) ) dna = dnaSeq.substring((begin += 3), end); 
		//## looking for a stop codon ####
		while ( (end < lgt) && ! isSTOP(lc(dna)) ) dna = dnaSeq.substring(begin, (end += 3)); 
	    }
	    if ( (! stop) && isSTOP(lc(dna)) ) dna = dnaSeq.substring(begin, (end -= 3)); 

	    fh = fh + "::" + (++begin) + "-" + end;
	    out = new BufferedWriter(new FileWriter(new File(basename + ".fna"))); out.write(fh); out.newLine(); out.write(dna);                                   out.newLine(); out.close();
	    out = new BufferedWriter(new FileWriter(new File(basename + ".faa"))); out.write(fh); out.newLine(); out.write(((orf)?dna2aa(dna):dna2aa(dna,start))); out.newLine(); out.close();

	    System.exit(0);
	}
	
	//### CDS/ORF expected on reverse complement ##########################
	dna = rc(dnaSeq.substring(--end, begin)); lgt = dnaSeq.length() - 2;
	//### ORF ########################
	if ( orf ) {
	    //## looking for a first stop codon ###
	    while ( (begin < lgt) && ! isSTOP(fc(dna)) ) dna = rc(dnaSeq.substring(end, (begin += 3))); 
	    if ( (! stop) && isSTOP(fc(dna)) ) dna = rc(dnaSeq.substring(end, (begin -= 3))); 
	    //## looking for a last stop codon ####
	    while ( (end >= 3) && ! isSTOP(lc(dna)) ) dna = rc(dnaSeq.substring((end -= 3), begin));
	}
	//### complete CDS ###############
	if ( fcds ) {
	    //## looking for a start codon ###
	    while ( (begin < lgt) && (! isin(c=fc(dna), start)) && ! isSTOP(c) ) dna = rc(dnaSeq.substring(end, (begin += 3))); 
	    if ( isSTOP(fc(dna)) ) dna = rc(dnaSeq.substring(end, (begin -= 3))); 
	    //## looking for a stop codon ####
	    while ( (end >= 3) && ! isSTOP(lc(dna)) ) dna = rc(dnaSeq.substring((end -= 3), begin)); 
	}
	if ( (! stop) && isSTOP(lc(dna)) ) dna = rc(dnaSeq.substring((end += 3), begin));

	fh = fh + "::" + begin + "-" + (++end);
	out = new BufferedWriter(new FileWriter(new File(basename + ".fna"))); out.write(fh); out.newLine(); out.write(dna);                                   out.newLine(); out.close();
	out = new BufferedWriter(new FileWriter(new File(basename + ".faa"))); out.write(fh); out.newLine(); out.write(((orf)?dna2aa(dna):dna2aa(dna,start))); out.newLine(); out.close();
    }

    // ##### methods to translate DNA sequence String to amino acid String #####
    final static String dna2aa ( String sequence ) {
	final int m = sequence.length() / 3; int b1 = -1, b2; StringBuilder sb = new StringBuilder(sequence.substring(0, m));
	while ( ++b1 < m ) { b2 = Arrays.binarySearch(CODON, sequence.substring(3*b1 , 3*(++b1))); sb.setCharAt(--b1 , ((b2 < 0) ? 'X' : AMINO_ACID[b2])); }
	return sb.toString().trim();
    }
    final static String dna2aa ( String sequence , String[] starts ) {
	final int m = sequence.length() / 3; int b1 = 0, b2; StringBuilder sb = new StringBuilder(sequence.substring(0, m));
	if ( isin(sequence.substring(3*b1 , 3*(++b1)), starts) ) sb.setCharAt(--b1, 'M'); else b1 = -1;
	while ( ++b1 < m ) { b2 = Arrays.binarySearch(CODON, sequence.substring(3*b1 , 3*(++b1))); sb.setCharAt(--b1 , ((b2 < 0) ? 'X' : AMINO_ACID[b2])); }
	return sb.toString().trim();
    }

    // ##### a method to translate DNA sequence String to its reverse complement #####
    final static String rc ( String sequence ) {
	int b1 = sequence.length(), b2 = -1; StringBuilder sb = new StringBuilder(sequence);
	while ( --b1 >= 0 ) 
	    switch ( sequence.charAt(b1) ) {
	    case 'A': sb.setCharAt(++b2 , 'T'); continue; case 'C': sb.setCharAt(++b2 , 'G'); continue;
	    case 'G': sb.setCharAt(++b2 , 'C'); continue; case 'T': sb.setCharAt(++b2 , 'A'); continue;
	    default:  sb.setCharAt(++b2 , '?'); break;
	    }
	return sb.toString().trim();
    }
    
    // ##### a method to get the first codon of the specified sequence #####
    static String fc (String sequence) { return sequence.substring(0,3); }

    // ##### a method to get the last codon of the specified sequence #####
    static String lc (String sequence) { return sequence.substring(sequence.length()-3); }
    
    // ##### a method to verify that the specified codon is a stop one #####
    static boolean isSTOP (String codon) { return (Arrays.binarySearch(CSTOP, codon) >= 0); }
    
    // ##### a method to verify that the specified codon is inside the specified array #####
    static boolean isin (String codon, String[] list) { return (Arrays.binarySearch(list, codon) >= 0); }
    
}
